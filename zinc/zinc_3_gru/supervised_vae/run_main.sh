#!/bin/bash

#SBATCH --partition=shared-gpu
#SBATCH -J zinc_3_gru_supervized
#SBATCH --mem=60000
#SBATCH -o jobname-out.o%j
#SBATCH --gres=gpu:1
#SBATCH -t 12:00:00
#BATCH --mail-user=amina.mollaysa@gmail.com
#SBATCH --mail-type=ALL
module load CUDA 
srun singularity exec --nv /home/aminanm0/icml_2019/sdvae_docker python main.py &> log.txt 
