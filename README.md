# Conditioning_on_weakly_informative_properties



# Structure of the repo
```sh
Conditioning_on_weakly_informative_properties_AISTAT2021  (root)
 |__  README.md
 |__  dropbox
 |__  zinc
 |__  |__ data
 |__  |__ zinc_3_gru
 |__  qm9_multiple_prop
 |__  |__ data
 |__  |__ qm9_3_gru
 |__  |__ data_process
```


# Main procedure 
    1. Install the dependencies 
    2. Download/prepare the data. 
    1. Run the supervised vae
    2. Run regressor
    3. Run the main model 


# To play with the code (detailed instruction)
## 1. Install the dependencies:
The current code depends on pytorch 0.5.0a0. All the dependency is discribed in requirement.txt file. You can also use the Dockerfile to set up all the dependency or directly pull the ready image from the following cite https://cloud.docker.com/u/2012913/repository/docker/2012913/sdvae_5_8

## 2. Download/prepare the data:
The cleaned dataset can be downloaded from this link: https://www.dropbox.com/sh/j0s1802x04jxnk7/AAA3fLJRnUE9vSe4K5Y2gfqVa?dl=0.  Refer to the repo structure discription in above to check if the data is placed correctly. 

### 1. QM9 dataset:
Download the dataset for QM9 and save it under the folder with folder name data: qm9_multiple_prop/data. 
The code that used for cleaning the data is also included in qm9_multiple_prop/data_process if it is needed. It takes input as smiles (QM9_clean_smi_train_smile.npy) and generate nine properties that are given in properties.py (num_rotatable_bonds,num_aromatic_rings,logP,qed,tpsa,bertz,.mol_weight,AtomCounter('F'),num_rings). If more properties are needed, it can be specified in properties.py file.

### 2. ZINC dataset:
Download the dataset for zinc and save it under the folder with the folder name zinc/data/data_278. 

## 3. Training the model
1. Run supervised VAE model to train your property predictor on the continous representation of the molecules which is the decoder ourout. 
   Enter inside the folder supervised_vae run main.py, this will train you a vanilla supervised vae model and save the result in the folder with name vanilla_supervised_vae.

2. Run the property predictor(regressor) on the ouput of  well trained supervised vae model. 
   Enter inside the folder regressor and run train_regressor.py, the resulted model will be saved in the folder with name regressor_pretrained.

3. Once you have trained the regressor, you can train the main model by runing the file qm9_multiple_prop/qm9_3_gru/main.py, the trained model will be saved under the folder named "model". The main model will load partially trained supvervised vae model and well trained property predictor (regressor) to initialize the vae model and property predictor. It optimize alternatively between the vae models and property predictor. 
