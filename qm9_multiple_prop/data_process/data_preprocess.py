from rdkit import Chem
import numpy as np
from joblib import Parallel, delayed
import sys, os
from pathlib import Path
from properties import PROPERTIES
import h5py


# get the properties
def prepare_data(smile, name):
    prop = []
    for i in range(smile.shape[0]):
        prop.append([prop(Chem.MolFromSmiles(smile[i])) for prop in PROPERTIES.values()])
        if len(prop) % 10000 == 0:
            print("processed %d strings" % len(prop))
    np.savez_compressed('../data/'+ name, smiles=smile, properties= prop)


#get the onehot encoding and masks
sys.path.append('%s/../qm9_3_gru/' % os.path.dirname(os.path.realpath('__file__')))
from cmd_args import cmd_args
cmd_args.grammar_file = './../qm9_3_gru/util/mol_zinc.grammar'


def run_job(L, name):
    chunk_size = 5000
    
    
    list_binary = Parallel(n_jobs=cmd_args.data_gen_threads, verbose=50)(
        delayed(process_chunk)(L[start: start + chunk_size])
        for start in range(0, len(L), chunk_size)
    )

    
    #process_chunk(L[start: start + chunk_size] for start in range(0, len(L), chunk_size))
   
    
    all_onehot = np.zeros((len(L), cmd_args.max_decode_steps, DECISION_DIM), dtype=np.byte)
    all_masks = np.zeros((len(L), cmd_args.max_decode_steps, DECISION_DIM), dtype=np.byte)

    for start, b_pair in zip( range(0, len(L), chunk_size), list_binary ):
        all_onehot[start: start + chunk_size, :, :] = b_pair[0]
        all_masks[start: start + chunk_size, :, :] = b_pair[1]

    #f_smiles = '.'.join(cmd_args.smiles_file.split('/')[-1].split('.')[0:-1])
    out_file = '%s/%s-%d.h5' % ('../data/', name, cmd_args.skip_deter)
    h5f = h5py.File(out_file, 'w')
    h5f.create_dataset('x', data=all_onehot)
    h5f.create_dataset('masks', data=all_masks)
    h5f.close()

sys.path.append('%s/../qm9_3_gru/util/' % os.path.dirname(os.path.realpath('__file__')))
import cfg_parser as parser
from mol_tree import AnnotatedTree2MolTree
from mol_util import DECISION_DIM
from attribute_tree_decoder import create_tree_decoder
from batch_make_att_masks import batch_make_att_masks
from tree_walker import OnehotBuilder 
def process_chunk(smiles_list):
    grammar = parser.Grammar(cmd_args.grammar_file)

    cfg_tree_list = []
    for smiles in smiles_list:
        ts = parser.parse(smiles, grammar)
        assert isinstance(ts, list) and len(ts) == 1

        n = AnnotatedTree2MolTree(ts[0])
        cfg_tree_list.append(n)

    walker = OnehotBuilder()
    tree_decoder = create_tree_decoder()
    onehot, masks = batch_make_att_masks(cfg_tree_list, tree_decoder, walker, dtype=np.byte)

    return (onehot, masks)


#### 1. load the smiles and get its property
train_smi = np.load('../data/QM9_clean_smi_train_smile.npy',allow_pickle=True)
prepare_data(train_smi, 'train')
test_smi = np.load('../data/QM9_clean_smi_test_smile.npy',allow_pickle=True)
prepare_data(test_smi, 'test')

### 2. get the binary representation and masks
run_job(test_smi, 'test_binary')
run_job(train_smi, 'train_binary')



