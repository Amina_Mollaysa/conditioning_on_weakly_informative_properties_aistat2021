import argparse
import time
import random
import numpy as np
import os
import torch 
import sys


from cmd_args import cmd_args
cmd_args.saved_model= './model_1/epoch-best.model'
cmd_args.training_data_dir = '../data/data_100'
generated_latent =  cmd_args.saved_model + '-gen_latent' 


train_smiles = np.load(cmd_args.training_data_dir + '/QM9_clean_smi_train_smile.npy', allow_pickle=True)
train_labels = np.load(cmd_args.training_data_dir + '/QM9_normalized_train_y.npy', allow_pickle=True)

# +
from model import CNNEncoder, MolVAE
vae = MolVAE()
device = 'cpu'
if cmd_args.mode == 'gpu':
    vae = vae.cuda()  
    device = 'cuda'

if cmd_args.saved_model is not None and cmd_args.saved_model != '':
        if os.path.isfile(cmd_args.saved_model):
            print('loading model from %s' % cmd_args.saved_model)
            vae.load_state_dict(torch.load(cmd_args.saved_model, map_location=device))


# -
from AttMolProxy import AttMolProxy


# +
model = AttMolProxy()
k = train_smiles.shape[0]
z = []
for i in range( int(k / cmd_args.batch_size)+1 ):
    smiles = train_smiles[i*cmd_args.batch_size: (i+1)*cmd_args.batch_size]
    z_,v = model.encode(smiles, use_random=False)
    z.append(z_)
    t = time.time()
    print(i)
    print(time.time() - t)
    
z = np.array(z)
z = np.vstack(z)
# -

np.save(generated_latent, z)

z.shape

generated_latent


