import sys
import numpy as np
import random
import torch
import argparse
from rdkit.Chem import rdmolops
from rdkit.Chem import MolFromSmiles, MolToSmiles
from rdkit.Chem import Descriptors
import networkx as nx
import util.sascorer

from cmd_args import cmd_args
cmd_args.training_data_dir = './../data/data_100'


## load the test data
train_smiles = np.load(cmd_args.training_data_dir + '/QM9_clean_smi_train_smile.npy', allow_pickle=True)
train_normalized_labels = np.load(cmd_args.training_data_dir + '/QM9_normalized_train_y.npy',allow_pickle=True)
train_labels = np.load(cmd_args.training_data_dir + '/QM9_clean_smi_train_y.npy', allow_pickle = True)

#n = train_smiles.shape[0]
#n = 5000
cycle = []
for i in range(train_smiles.shape[0]):
    cycle_list = nx.cycle_basis(nx.Graph(rdmolops.GetAdjacencyMatrix(MolFromSmiles(train_smiles[ i ]))))
    if len(cycle_list) == 0:
        cycle_length = 0
    else:
        cycle_length = max([ len(j) for j in cycle_list ])
    if cycle_length <= 6:
        cycle_length = 0
    else:
        cycle_length = cycle_length - 6

    current_cycle_score = -cycle_length
    cycle.append(current_cycle_score)

np.save(cmd_args.training_data_dir + '/cycle', np.array(cycle))


